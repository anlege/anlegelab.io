---
title: 浏览器mq发送乱码
---

主要是加 [Properties](https://so.csdn.net/so/search?q=Properties&spm=1001.2101.3001.7020) 配置

网络上很多文章提到了 content_type，我配置了还是收到数字，还需要加上 content_encoding

content_type ：text/plain

content_encoding ：UTF-8



![](https://tcs-devops.aliyuncs.com/storage/112l23be935c58fb4644e5ee1e1ee19b071d?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MjkwMiwiaWF0IjoxNjc4OTU4MTAyLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMmwyM2JlOTM1YzU4ZmI0NjQ0ZTVlZTFlMWVlMTliMDcxZCJ9.3ZhZZbEORpGecr747UI0hC8HnNWilUg6wiVWELy8Y_U&download=image.png "")

