---
title: 增强swagger（Gradle）
---
### build.gradle文件添加依赖

```text
implementation group: 'com.github.xiaoymin', name: 'knife4j-spring-boot-starter', version: '2.0.4'
```

WebMvcApiConfig 文件   添加红色的两行代码

@Override

    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");

        registry.addResourceHandler("swagger-ui.html")

                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")

                .addResourceLocations("classpath:/META-INF/resources/webjars/");

        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

        //super.addResourceHandlers(registry);

    }