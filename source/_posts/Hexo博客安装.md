---
title: hexo博客安装
---
npm install hexo-cli -g

hexo init 

如果报错就是nodeJs 版本太低



### 生成静态页面

hexo g

### 启动本地服务

hexo s

美化个人博客

## 博客主题设置

### 克隆主题

- 在项目根目录下的 **themes** 文件中，打开 **Git Bash** ，用命令行克隆下新的主题。我这里用的 Next 主题，需要其他主题的自己百度找。

```text
git clone https://github.com/theme-next/hexo-theme-next.git
```

配置主题

- 用文本的方式打开项目根目录下的 _config.yml 配置文件，找到 **theme** 把原来默认的 ~~**landscape**~~ 主题名字，改成刚刚克隆的主题名字（主题名字为上图中文件夹的名字）。

![](https://tcs-devops.aliyuncs.com/storage/11236f013f4f2b39a3d3c8d03ec586dab36d?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzczMSwiaWF0IjoxNjc4OTU4OTMxLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjM2ZjAxM2Y0ZjJiMzlhM2QzYzhkMDNlYzU4NmRhYjM2ZCJ9.fDx1zd_2jWKSgT0PjlRzfoxT9e-wW6DsMFoce9R65qs&download=image.png "")

测试主题

- 重新回到项目根目录下，打开 **Git Bath** ，用命令行启动服务器。在浏览器访问 [http://localhost:4000](http://localhost:4000/)

