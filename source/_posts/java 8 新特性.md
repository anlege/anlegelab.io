---
title: java 8 新特性
---
### Consumer

Consumer是一个函数式编程接口； 顾名思义，Consumer的意思就是消费，即针对某个东西我们来使用它，因此它包含有一个有输入而无输出的accept接口方法；

除accept方法，它还包含有andThen这个方法；

```text
default Consumer<T> andThen(Consumer<? super T> after) {
    	Objects.requireNonNull(after);
    	return (T t) -> { accept(t); after.accept(t); };
}
```

可见 andThen 这个方法就是指定在调用当前Consumer后是否还要调用其它的Consumer；andThen是默认调用接口

```text
public static void consumerTest() {
    	Consumer f = System.out::println;
    	Consumer f2 = n -> System.out.println(n + "-F2");

    	//执行完F后再执行F2的Accept方法
    	f.andThen(f2).accept("test");

    	//连续执行F的Accept方法
    	f.andThen(f).andThen(f).andThen(f).accept("test1");
}
```

Consumer还有多个其他的接口,具体有以下几种，在使用函数式接口时，若有提供具体类型的接口，就尽量使用此接口，因为具体的类型指定可以避免装箱拆箱时所带来的额外资源消耗

BiConsumer　　　　接收两个参数

IntConsumer　　　　接收一个int参数

LongConsumer　　　　接收一个long参数

DoubleConsumer　　　　收一个double参数

ObjIntConsumer　　　　接收两个参数  T，int

ObjLongConsumer　　　　接收两个参数  T，long

ObjDoubleConsumer　　　　接收两个参数  T,，double



### Function

Function也是一个函数式编程接口；它代表的含义是“函数”，而函数经常是有输入输出的，因此它含有一个apply方法，包含一个输入与一个输出；除apply方法外，它还有compose与andThen及indentity三个方法，其使用见下述示例；

```text
Function<Integer,Integer> f1 = s -> s+2;
Function<Integer,Integer> f2 = s -> s*2;
System.out.println(f1.apply(20));;			//22
System.out.println(f2.apply(20));;			//40
System.out.println(f1.compose(f2).apply(20));	//42	先执f2把f2当做f1的参数
System.out.println(f1.andThen(f2).apply(20));	//44	先执f1把f1当做f2的参数
System.out.println(Function.identity().apply("a"));//identity方法会返回一个不进行任何处理的Function，即输出与输入值相等； 
```

### Predicate

Predicate为函数式接口，predicate的中文意思是“断定”，即判断的意思，判断某个东西是否满足某种条件； 因此它包含test方法，根据输入值来做逻辑判断，其结果为True或者False。

可以用来做字符串判断

```text
Predicate<String> p = s -> {
    String ip = "192.168";
    int i = s.indexOf(ip);
    if(i == 0){
        return true;
    }else{
        return false;
    }
};
System.out.println(p.test("192.168.0.141"));; 	//true
```

