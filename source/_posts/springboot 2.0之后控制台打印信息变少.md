---
title: springBoot 2.0之后控制台打印信息变少
---

添加配置

```text
logging:
  level:
    web: trace
    root: info
    org.mybatis: debug
    java.sql: debug
    org.springframework.web: trace

```

