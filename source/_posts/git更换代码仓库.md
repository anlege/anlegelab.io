---
title: git更换代码仓库
---
找到项目根目录

git init 		初始化

修改.git 文件夹下config文件

在后面添加

[remote "origin"]

	url = http://gitlab.bafang-e.com:7020/bafang/bfbase.git

	fetch = +refs/heads/*:refs/remotes/origin/*

[branch "master"]

	remote = origin

	merge = refs/heads/master

蓝色是需要替换的仓库路径



找到凭据管理器 修改仓库账号密码

![](https://tcs-devops.aliyuncs.com/storage/112344bc5bdb6deabae63a8ba36d9c184923?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2Mzg2MywiaWF0IjoxNjc4OTU5MDYzLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjM0NGJjNWJkYjZkZWFiYWU2M2E4YmEzNmQ5YzE4NDkyMyJ9.wq_paIZB9onnxMJJWk8WJ2WcWt3qr7fVdbuuoKH3Bn8&download=image.png "")

添加到本地仓库

git add .

提交

git commit -m "第一次提交"

推送

git push

强制推送

**git push -f origin master**