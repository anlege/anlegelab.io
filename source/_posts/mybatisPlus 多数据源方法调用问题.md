
---
title: mybatisplus 多数据源方法调用问题
---
当一个数据源方法被 @Transactional 修饰并且调用另一个数据源的方法时要在另一个方法上面

加上

```text
@Transactional(propagation = Propagation.NOT_SUPPORTED)
```

```text
package com.leanin.follow.service.impl.his;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leanin.follow.common.CommonCode;
import com.leanin.follow.common.ObjectResultResponse;
import com.leanin.follow.common.ResponseResult;
import com.leanin.follow.dto.PatientQueryDto;
import com.leanin.follow.entity.OnlineEdu;
import com.leanin.follow.entity.his.PatsInfoOuthos;
import com.leanin.follow.mapper.his.PatsInfoOuthosMapper;
import com.leanin.follow.service.his.IPatsInfoOuthosService;
import com.leanin.follow.utils.CustomUtil;
import com.leanin.follow.vo.ChronicDiseasePatientVo;
import com.leanin.follow.vo.PlanPatientVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangCong
 * @since 2020-01-20
 */
@Slf4j
@Service
@DS("his")
public class PatsInfoOuthosServiceImpl extends ServiceImpl<PatsInfoOuthosMapper, PatsInfoOuthos> implements IPatsInfoOuthosService {

    @Resource
    private PatsInfoOuthosMapper patsInfoOuthosMapper;

    @Override
    public List<PlanPatientVo> findOutHosPatient(PatientQueryDto paramMap) {
        return patsInfoOuthosMapper.findOutHosPatient(paramMap);
    }

    @Override
    public Map<String,Object> preOutHosPatient(PatientQueryDto paramMap,Integer pageSize,Integer pageNum) {
        Page<PlanPatientVo> page = new Page<>(pageNum, pageSize);
        List<PlanPatientVo> list = patsInfoOuthosMapper.preOutHosPatient(page,paramMap);
        page.setRecords(list);
        Map<String,Object> result = new HashMap<>();
        result.put("totalCount", page.getTotal());
        result.put("list", list);
        return result;
    }

    @Override
    public ResponseResult findOutByParam(PatientQueryDto params) {
        log.info("查询门诊记录条件:" + params.toString());
        if(StringUtils.isNotBlank(params.getDiagnosis()) && !CustomUtil.isContainChinese(params.getDiagnosis())){
            params.setDiagnosisIcd(params.getDiagnosis());
            params.setDiagnosis(null);
        }
        List<PatsInfoOuthos> pageList = findOutListByParam(params, (params.getPageNum()-1)*params.getPageSize(), params.getPageSize());
        int count = findOutCountByParam(params);
        Map<String,Object> result =new HashMap<>();
        result.put("totalCount",count);
        result.put("list",pageList);
        return new ObjectResultResponse(CommonCode.SUCCESS,result);
    }

    @Async
    @Override
    public List<PatsInfoOuthos> findOutListByParam(PatientQueryDto params, Integer limitStart, Integer limitEnd){
        return patsInfoOuthosMapper.findOutByParam(params, limitStart, limitEnd);
    }

    @Async
    @Override
    public Integer findOutCountByParam(PatientQueryDto params){
        return patsInfoOuthosMapper.findOutCountByParam(params);
    }

    @Override
    public List<ChronicDiseasePatientVo> getChronicDiseasePatient(Map<String, Object> params) {
        return patsInfoOuthosMapper.getChronicDiseasePatient(params);
    }

    @Override
    public List<OnlineEdu> getEduPatientList(List<String> patientIdList, List<String> deptCodeList) {
        return patsInfoOuthosMapper.getEduPatientList(patientIdList, deptCodeList);
    }

    @Override
    public List<ChronicDiseasePatientVo> getZLChronicDiseasePatient(LocalDateTime time) {
        return patsInfoOuthosMapper.getZLChronicDiseasePatient(time);
    }

    @Override
    public List<PatsInfoOuthos> getPatsInfoOuthosList(String patientId) {
        List<PatsInfoOuthos> patsInfoOuthosList = new ArrayList<>();
        if (StringUtils.isBlank(patientId)){
            return patsInfoOuthosList;
        }
        patsInfoOuthosList = this.list(new LambdaQueryWrapper<PatsInfoOuthos>().eq(PatsInfoOuthos::getPatientId, patientId).orderByDesc(PatsInfoOuthos::getClinicTime));
        return patsInfoOuthosList;
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public PatsInfoOuthos getPatsInfoOuthos(String hosNo) {
        PatsInfoOuthos patsInfoOuthos = patsInfoOuthosMapper.selectOneByHosNo(hosNo);
        return patsInfoOuthos;
    }

}

```

