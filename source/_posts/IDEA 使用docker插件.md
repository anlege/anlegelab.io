---
title: IDEA 使用docker插件
---
### 更新docker插件为最新插件

[__https://thoughts.aliyun.com/workspaces/603848df664d9e001fb72322/docs/60669d35e87e05000190ec6f__](https://thoughts.aliyun.com/workspaces/603848df664d9e001fb72322/docs/60669d35e87e05000190ec6f)

### 安装docker插件

![](https://tcs-devops.aliyuncs.com/storage/112401799047af0d75963ea8dc0dec5a79e5?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjQwMTc5OTA0N2FmMGQ3NTk2M2VhOGRjMGRlYzVhNzllNSJ9.AeKUSwurzu_MwOQNzBRP4tieUXv06MsjVldZJdorUAk&download=image.png "")

### 配置docker外部连接

先开通docker  2375端口   [__https://thoughts.aliyun.com/workspaces/603848df664d9e001fb72322/docs/606675ed35080b00013fdd74__](https://thoughts.aliyun.com/workspaces/603848df664d9e001fb72322/docs/606675ed35080b00013fdd74)

配置docker插件

![](https://tcs-devops.aliyuncs.com/storage/112472b0399b684d8c016cb022d9c30f3ad2?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjQ3MmIwMzk5YjY4NGQ4YzAxNmNiMDIyZDljMzBmM2FkMiJ9.BGdv0Dauw_IcAPc76fH2VBMrXXHuwcyGzZtSQ4l_280&download=image.png "")

### 配置docker 镜像上传地址

dockerhub    [__https://hub.docker.com/__](https://hub.docker.com/)

![](https://tcs-devops.aliyuncs.com/storage/11249a5410364c64c2bf097c0d3a183c074c?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjQ5YTU0MTAzNjRjNjRjMmJmMDk3YzBkM2ExODNjMDc0YyJ9.GkZza2pTSEZkfTlZn110X9DNpTdrIyWOUyJ97OO--bY&download=image.png "")

阿里云

![](https://tcs-devops.aliyuncs.com/storage/1124cb664a8022e22235a9a2e8beabc81488?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjRjYjY2NGE4MDIyZTIyMjM1YTlhMmU4YmVhYmM4MTQ4OCJ9.zxzUBek0_YJUNicavYvEONZbcalDX6udSC-L1QeSrbQ&download=image.png "")

编写Dockefile文件放到根目录下

```text
FROM adoptopenjdk:11-jre-hotspot as builder
WORKDIR application
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} /application/
RUN java -Djarmode=layertools -jar halo-1.4.7.jar extract

################################

FROM adoptopenjdk:11-jre-hotspot
MAINTAINER johnniang <johnniang@fastmail.com>
WORKDIR application
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./

# JVM_XMS and JVM_XMX configs deprecated for removal in halov1.4.4
ENV JVM_XMS="256m" \
    JVM_XMX="256m" \
    JVM_OPTS="-Xmx256m -Xms256m" \
    TZ=Asia/Shanghai

RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone

ENTRYPOINT java -Xms${JVM_XMS} -Xmx${JVM_XMX} ${JVM_OPTS} -Djava.security.egd=file:/dev/./urandom org.springframework.boot.loader.JarLauncher
```

运行docker

![](https://tcs-devops.aliyuncs.com/storage/1124fcc4f8b4484b9af6ec1b277b4a6f0d09?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjRmY2M0ZjhiNDQ4NGI5YWY2ZWMxYjI3N2I0YTZmMGQwOSJ9.cepjJNEMM3r5tXvNjCVDYFj2WrgX6zfgMzrxfCQZ8Rg&download=image.png "")

上面有容器和镜像可以操作

![](https://tcs-devops.aliyuncs.com/storage/11247730bd3282c7e22d043903d59f3721e7?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjQ3NzMwYmQzMjgyYzdlMjJkMDQzOTAzZDU5ZjM3MjFlNyJ9.KmLky44k_m99nOHUfZjqeOHPPIbYrwIEVdkwwYGTtyk&download=image.png "")

运行Dockerfile文件生成镜像

右击镜像可创建容器 或者上传镜像到dockerhub 或者阿里云

创建容器配置文件

![](https://tcs-devops.aliyuncs.com/storage/1124ac1f5de9117ea29c26df9a5f91370e72?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9hcHBJZCI6IjVlNzQ4MmQ2MjE1MjJiZDVjN2Y5YjMzNSIsIl9vcmdhbml6YXRpb25JZCI6IiIsImV4cCI6MTY3OTU2MzY4OCwiaWF0IjoxNjc4OTU4ODg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzExMjRhYzFmNWRlOTExN2VhMjljMjZkZjlhNWY5MTM3MGU3MiJ9.WbZj3He0AGwkkgi-ld4XwjUlbGidfbiikmaRa2hulRk&download=image.png "")

