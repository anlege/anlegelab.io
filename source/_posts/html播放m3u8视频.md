---
title: html播放m3u8视频
---
[GitHub - video-dev/hls.js: JavaScript HLS client using Media Source Extension](https://github.com/video-dev/hls.js)

```html
<!DOCTYPE HTML>
<html>
<body>

<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
<!-- Or if you want a more recent alpha version -->
<!-- <script src="https://cdn.jsdelivr.net/npm/hls.js@alpha"></script> -->
<video id="video"></video>
<script>
  var video = document.getElementById('video');
  var videoSrc = 'https://cdn-youku-com.diudie.com/series/284/index.m3u8';
  //
  // First check for native browser HLS support
  //
  if (video.canPlayType('application/vnd.apple.mpegurl')) {
    video.src = videoSrc;
  //
  // If no native HLS support, check if hls.js is supported
  //
  } else if (Hls.isSupported()) {
    var hls = new Hls();
    hls.loadSource(videoSrc);
    hls.attachMedia(video);
  }
</script>
</body>
</html>


```

[Chimee - 可扩展的组件化H5播放器框架](http://chimee.org/)

```html
<!DOCTYPE HTML>
<html>
<body>

<script src="https://s3.ssl.qhres.com/!fd546749/chimee-player.browser.js" type="text/javascript"></script>
<!-- Or if you want a more recent alpha version -->
<!-- <script src="https://cdn.jsdelivr.net/npm/hls.js@alpha"></script> -->
<div id="wrapper"></div>
	<script>
		const chimee = new ChimeePlayer({
  wrapper: '#wrapper',
  src: 'https://cdn-youku-com.diudie.com/series/285/index.m3u8',
  controls: false,
  autoplay: true,
});
	</script>
</body>
</html>


```

