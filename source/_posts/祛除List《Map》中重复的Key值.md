
---
title: 去除List<Map>中重复的key值 
---
```none
 List<Map> list = new ArrayList();
    Map<String,Object> mapAll = new HashMap();
    for(Map<String,Object> map1:list){
        for(Map.Entry<String, Object> entry:map1.entrySet()){
            String key = entry.getKey();
            Object value = entry.getValue();
            Object all = mapAll.get(entry.getKey());
            if(all == null){
                mapAll.put(key, value);
            }else{
                all = value+","+all;
                mapAll.put(key, all);
            }
        }
    }

```

