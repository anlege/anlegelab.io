---
title: java导入Excel数据 
---
引入依赖

```xml
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>3.13</version>
        </dependency>
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>3.13</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.6</version>
        </dependency>
```

java代码

```none
String title="";
String label="";
String answer="";
//(1)得到上传的表
Workbook workbook = WorkbookFactory.create(file.getInputStream());
//(2)获取工作表
Sheet sheet = workbook.getSheetAt(0);       //.getSheetAt(0)  通过下标获取工作表，.getSheet("工作表")通过名字获取工作表
//(3)
DataFormatter formatter = new DataFormatter();
//(4)通过迭代器便利工作表
Iterator<Row> rowIterator = sheet.iterator();
while (rowIterator.hasNext()) {
    //获取下一行
    Row row = rowIterator.next();
    //获取表的总行数
    int num = row.getRowNum();
    if (num <= 2) {
        continue;
    }
    //通过迭代器便利每一列数据
    Iterator<Cell> cellIterator = row.cellIterator();
    while (cellIterator.hasNext()) {
        Cell cell = cellIterator.next();
        //格式化单元格数据
        String cellContent = formatter.formatCellValue(cell);

        switch (cell.getColumnIndex()) {
            case 0:
                title = cellContent.trim();
                break;
            case 1:
                label = cellContent.trim();
                break;
            case 2:
                answer = cellContent.trim();
                break;
        }
    }
}

```

