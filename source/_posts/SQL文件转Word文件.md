---
title: Sql文件转Word文件 
---
```java
package com.example.demo;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblGrid;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblGridCol;

import java.io.*;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        try {
            createDoc("e:\\besst.docx", "e:\\online-test.sql");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String getTableName(String sql) {
        String result = null;
        if (sql.contains("(")) {
            result = sql.substring(6, sql.indexOf("(") - 1).trim();
            result = result.substring(1, result.length() - 1);
            if (sql.contains("comment on table")) {
                String s = sql.split(";")[1];
                result += "(" + s.substring(s.indexOf("'") + 1, s.lastIndexOf("'")) + ")";
            }
        }
        return result;
    }

    private static void insertCells(XWPFDocument doc, String sql) {

        //获得field属性的sql
        String substring = sql.substring(sql.indexOf("(") + 1, sql.lastIndexOf(")"));
        String[] split = substring.split(",\n");
        //创建表格
        XWPFTable table = doc.createTable(split.length, 5);
        //设置表格宽度
        setW(table, 5);
        //第一行数据插入
        insertHeader(table);
        //获得备注的map
        Map<String, String> map = getCommentInfo(sql);
        //表字段数据插入
        for (int i = 0; i < split.length - 1; i++) {
            int biao = 0;
            int index = 0;
            String[] split1 = split[i].split(" ");
            List<XWPFTableCell> tableCells = table.getRow(i + 1).getTableCells();
            //遍历获得字段信息
            for (int j = 0; j < split1.length; j++) {
                if (split1[j].trim().length() > 0) {
                    if (split1[j].trim().indexOf("PRIMARY") == -1 && split1[j].trim().indexOf("UNIQUE") == -1 && split1[j].trim().indexOf("KEY") == -1 && split1[j].trim().indexOf("CONSTRAINT") == -1 && split1[j].trim().indexOf("FULLTEXT") == -1) {
                        if (index == 0) {
                            //设置属性名
                            String name = split1[j].trim().substring(1, split1[j].trim().length() - 1);
                            tableCells.get(0).setText(name);
                            //备注
                            String remark = map.get(name) != null ? map.get(name) : "";
                            tableCells.get(4).setText(remark);
                            index = 1;
                        } else if (index == 1) {
                            //类型
                            String type = split1[j].trim();
                            //长度
                            String length = "";
                            if (type.contains("(")) {
                                length = type.substring(type.indexOf("(") + 1, type.indexOf(")"));
                                type = type.substring(0, type.indexOf("("));
                            }
                            tableCells.get(1).setText(type);
                            tableCells.get(2).setText(length);
                            break;
                        }
                    }else{
                        biao = 1;
                        break;
                    }
                }
            }
            if(biao != 1){
                //是否为null
                if (split[i].contains("not null")) {
                    tableCells.get(3).setText("是");
                } else {
                    tableCells.get(3).setText("否");
                }
            }
        }
    }

    private static Map<String, String> getCommentInfo(String sql) {
        Map<String, String> map = new HashMap<String, String>();
        String[] split = sql.split(";");
        for (int i = 1; i < split.length; i++) {
            String row = split[i];
            if (row.contains(" column ")) {
                map.put(row.substring(row.indexOf(".") + 1, row.indexOf(" is")), row.substring(row.indexOf("'") + 1, row.lastIndexOf("'")));
            }
        }
        return map;
    }

    private static void insertHeader(XWPFTable table) {
        List<XWPFTableCell> tableCells = table.getRow(0).getTableCells();
        tableCells.get(0).setText("字段名称");
        tableCells.get(1).setText("类型");
        tableCells.get(2).setText("长度");
        tableCells.get(3).setText("是否必填");
        tableCells.get(4).setText("备注");
    }

    private static void createDoc(String docDir, String sqlDir) throws IOException {
        //创建新的word
        XWPFDocument doc = new XWPFDocument();
        //从1.sql中拿
        String sql = getSql(sqlDir);
        //截成表
        String[] creates = sql.split("CREATE");
        for (int i = 1; i < creates.length; i++) {
            if (creates[i].contains("TABLE")) {
                insertItem(doc, creates[i]);
            }
        }

        try {
            //导出到本地,E:\demo.docx
            FileOutputStream out = new FileOutputStream(docDir);
            try {
                doc.write(out);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static String getSql(String dir) throws IOException {
        File file = new File(dir);
        String result = "";
        InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF8");
        BufferedReader br = new BufferedReader(isr);//构造一个BufferedReader类来读取文件
        String s = null;
        while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
            result = result + "\n" + s;
        }
        br.close();
        System.out.println(result);
        return new String(result);
    }

    private static void insertItem(XWPFDocument doc, String sql) {
        //创建段落
        XWPFParagraph p1 = doc.createParagraph();
        //写入段落内容
        XWPFRun run = p1.createRun();
        run.setText(getTableName(sql));
        //插入字段
        insertCells(doc, sql);
    }


    private static void setW(XWPFTable table, int colNum) {
        CTTbl ttbl = table.getCTTbl();
        CTTblGrid tblGrid = ttbl.getTblGrid() != null ? ttbl.getTblGrid()
                : ttbl.addNewTblGrid();
        for (int i = 0; i < colNum; i++) {
            CTTblGridCol gridCol = tblGrid.addNewGridCol();
            gridCol.setW(new BigInteger("" + 1700));
        }
        table.setCellMargins(20, 20, 20, 20);
    }
}


```

